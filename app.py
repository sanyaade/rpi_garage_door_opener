from flask import Flask, render_template
import time # remove after GPIO implemented
from config import gpio_pin, activation_time

try:
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gpio_pin, GPIO.OUT)
except ImportError:
    print "could not import RPi.GPIO module!  Make sure you have the module installed."
    # for developing on non-rpi machines
    from dummy_gpio import GPIO


app = Flask(__name__)

@app.route("/")
def home():
    return render_template("base.html")

@app.route("/activate")
def activate():
    GPIO.output(gpio_pin, True)
    time.sleep(activation_time)
    GPIO.output(gpio_pin, False)
    return "{success:true}"

if __name__ == "__main__":
    try:
        app.run(host='0.0.0.0', port=80)
    except:
        app.run(host='0.0.0.0')