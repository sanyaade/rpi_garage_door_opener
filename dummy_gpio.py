class GPIO():
    @classmethod
    def output(self, pin, value):
        message = "RELAY IS NOT ACTUALLY ACTIVATING. MAKE SURE YOU HAVE THE RPi.GPIO MODULE INSTALLED."
        if value:
            print "Activating relay -   pin #%s. --- %s" % (pin, message)
        else:
            print "Deactivating relay - pin #%s. --- %s" % (pin, message)